# silly blocks

def reverser(&block)
  str = block.call
  str.split(' ').map(&:reverse).join(' ')
end

def adder(n=1, &prc)
  n + prc.call
end

def repeater(n=1, &prc)
  n.times do |i|
    prc.call
  end
  
end
