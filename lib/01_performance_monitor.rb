# perf

require 'time'

def measure(m=1, &prc)
  intervals = []
  m.times do
    before = Time.now
    prc.call
    after = Time.now
    intervals << after - before
  end
  intervals.reduce(:+) / intervals.length.to_f
end
